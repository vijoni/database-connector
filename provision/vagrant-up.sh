#!/usr/bin/env bash

sudo apt update
if ! hash jq 2>/dev/null; then
  sudo apt install -y jq vim curl
fi

#install php
sudo apt install -y lsb-release ca-certificates apt-transport-https software-properties-common gnupg2
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/sury-php.list
wget -qO - https://packages.sury.org/php/apt.gpg | sudo apt-key add -

sudo apt update
sudo apt install -y php8.1 \
php8.1-curl \
php8.1-bcmath \
php8.1-gd \
php8.1-mbstring \
php8.1-xdebug \
php8.1-xml \
php8.1-zip \
php8.1-pgsql

sudo cp /provision/xdebug.ini /etc/php/8.1/mods-available/

#install postgresql
echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -sc)-pgdg 14" | sudo tee /etc/apt/sources.list.d/pgdg.list
wget -qO - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

sudo apt update
sudo apt install -y postgresql postgresql-contrib
sudo systemctl start postgresql.service
sudo systemctl enable postgresql.service
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'postgres'"

# provision the app
sh -c "cd /app && XDEBUG_MODE=off ./composer install"
