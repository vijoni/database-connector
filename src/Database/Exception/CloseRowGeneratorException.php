<?php

declare(strict_types=1);

namespace Vijoni\Database\Exception;

class CloseRowGeneratorException extends \Exception
{
}
