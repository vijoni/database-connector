<?php

declare(strict_types=1);

namespace Vijoni\Database\Client;

class QueryResultValue
{
  public function __construct(
    private mixed $value = null,
    private bool $notFound = false
  ) {
  }

  public function getValue(): mixed
  {
    return $this->value;
  }

  public function notFound(): bool
  {
    return $this->notFound;
  }
}
