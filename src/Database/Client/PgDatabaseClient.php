<?php

declare(strict_types=1);

namespace Vijoni\Database\Client;

use Vijoni\Database\Exception\CloseRowGeneratorException;
use Vijoni\Database\Exception\DatabaseException;
use Vijoni\Database\QueryBuilder\PgQueryBuilder;
use Vijoni\Database\QueryBuilder\QueryBuilder;

class PgDatabaseClient extends DatabaseClient
{
  // @phpstan-ignore-next-line
  public function connect(string $dbUrl, string $dbVariant)
  {
    $urlTokens = parse_url($dbUrl);
    $dbName = ltrim($urlTokens['path'] ?? '', '/');

    $connectionString = $this->createConnectionString(
      $urlTokens['host'] ?? '',
      $urlTokens['port'] ?? 0,
      $dbName,
      $urlTokens['user'] ?? '',
      $urlTokens['pass'] ?? '',
    );

    $connection = pg_connect($connectionString, PGSQL_CONNECT_FORCE_NEW);
    $connection === false && throw new DatabaseException("Could not connect to {$dbVariant} database [{$dbName}]");

    return $connection;
  }

  // @phpstan-ignore-next-line
  protected function execute(string $query, string $comment = '')
  {
    $queryBuilder = $this->newQueryBuilder($query, [], [], $comment);
    $sqlQuery = $queryBuilder->build();

    try {
      $resultResource = pg_query($this->connection, $sqlQuery);
    } catch (\Throwable $e) {
      $errorMsg = $e->getMessage();
      throw new DatabaseException("Query failed [{$queryBuilder->getComment()}]\n{$errorMsg}");
    }

    if ($resultResource === false) {
      $this->reportSqlError($sqlQuery, $queryBuilder);
    }

    return $resultResource;
  }

  public function queryNative(string $query, string $comment = ''): \Generator
  {
    $resultResource = $this->execute($query, $comment);

    return (static function () use ($resultResource): \Generator {
      try {
        while ($row = pg_fetch_assoc($resultResource)) {
          yield $row;
        }

        pg_free_result($resultResource);
      } catch (CloseRowGeneratorException $e) {
        pg_free_result($resultResource);
      }
    })();
  }

  protected function createQuerySingleResultValue(\Generator $rowGenerator): QueryResultValue
  {
    $row = $rowGenerator->current();
    if (empty($row)) {
      return new QueryResultValue(null, true);
    }

    $rowGenerator->throw(new CloseRowGeneratorException());

    return new QueryResultValue($row);
  }

  public function newQueryBuilder(
    string $query = '',
    array $replaceValues = [],
    array $aliases = [],
    string $comment = ''
  ): QueryBuilder {
    return new PgQueryBuilder($this->con(), $query, $replaceValues, $aliases, $comment);
  }

  protected function startTransactionImplementation(): void
  {
    $this->queryNative('START TRANSACTION;', 'start transaction');
  }

  protected function commitImplementation(): void
  {
    $this->queryNative('COMMIT;', 'commit transaction');
  }

  protected function rollbackImplementation(): void
  {
    $this->queryNative('ROLLBACK;', 'rollback transaction');
  }

  private function createConnectionString(string $host, int $port, string $name, string $user, string $pass): string
  {
    return <<<CONNNECTION_STRING
host={$host}
port={$port}
dbname={$name}
user={$user}
password={$pass}
connect_timeout=5
options='--client_encoding=UTF8'
CONNNECTION_STRING;
  }

  private function reportSqlError(string $sqlQuery, QueryBuilder $queryBuilder): void
  {
    if ($this->isInsideTransaction()) {
      $this->forceRollback();
    }

    pg_send_query($this->connection, $sqlQuery);
    $result = pg_get_result($this->connection);
    if ($result !== false) { // for static analysis make sure it's not false
      $errorMsg = pg_result_error($result);
      pg_free_result($result);

      throw new DatabaseException("Query failed [{$queryBuilder->getComment()}]\n{$errorMsg}");
    }
  }
}
