<?php

declare(strict_types=1);

namespace Vijoni\Database\Client;

use Vijoni\Database\Exception\DatabaseTransactionException;
use Vijoni\Database\Exception\DatabaseValueNotFoundException;
use Vijoni\Database\QueryBuilder\QueryBuilder;

abstract class DatabaseClient
{
  private const SLAVE_DB = 'read';

  private const MASTER_DB = 'write';

  private const TX_COUNTER_INITIAL_VALUE = 0;

  private array $dbUrlPool = [];

  private array $connectionPool = [];

  protected $connection; // @phpstan-ignore-line

  protected int $txCounter = self::TX_COUNTER_INITIAL_VALUE;

  protected bool $hasRolledback = false;

  public function __construct(string $masterDbUrl, string $slaveDbUrl = '')
  {
    $this->dbUrlPool[self::MASTER_DB] = $masterDbUrl;
    $this->dbUrlPool[self::SLAVE_DB] = $slaveDbUrl;
  }

  public function init(): void
  {
    if ($this->dbUrlPool[self::SLAVE_DB] !== '') {
      $this->useConnection(self::SLAVE_DB);
    } else {
      $this->useConnection(self::MASTER_DB);
    }
  }

  // @phpstan-ignore-next-line
  abstract protected function connect(string $dbUrl, string $dbVariant);

  private function useConnection(string $dbVariant): void
  {
    if (!isset($this->connectionPool[$dbVariant])) {
      $dbUrl = $this->dbUrlPool[$dbVariant];
      $this->connectionPool[$dbVariant] = $this->connect($dbUrl, $dbVariant);
    }

    $this->connection = $this->connectionPool[$dbVariant];
  }

  public function useWriteConnection(): void
  {
    $this->useConnection(self::MASTER_DB);
  }

  public function con() // @phpstan-ignore-line
  {
    return $this->connection;
  }

  // @phpstan-ignore-next-line
  abstract protected function execute(string $query, string $comment = '');

  abstract public function queryNative(string $query, string $comment = ''): \Generator;

  public function queryNativeAll(string $string, string $comment = ''): array
  {
    $rowGenerator = $this->queryNative($string, $comment);

    return iterator_to_array($rowGenerator, true);
  }

  public function query(QueryBuilder $queryBuilder): \Generator
  {
    return $this->queryNative($queryBuilder->build(), $queryBuilder->getComment());
  }

  public function queryAll(QueryBuilder $queryBuilder): array
  {
    $rowGenerator =  $this->query($queryBuilder);

    return iterator_to_array($rowGenerator, true);
  }

  abstract protected function createQuerySingleResultValue(\Generator $rowGenerator): QueryResultValue;

  abstract public function newQueryBuilder(
    string $query = '',
    array $replaceValues = [],
    array $aliases = [],
    string $comment = ''
  ): QueryBuilder;

  public function queryStringValue(QueryBuilder $queryBuilder): string|null
  {
    $value = $this->querySingleValue($queryBuilder);

    return is_null($value) ? null : strval($value);
  }

  public function queryIntValue(QueryBuilder $queryBuilder): int|null
  {
    $value = $this->querySingleValue($queryBuilder);

    return is_null($value) ? null : intval($value);
  }

  public function queryFloatValue(QueryBuilder $queryBuilder): float|null
  {
    $value = $this->querySingleValue($queryBuilder);

    return is_null($value) ? null : floatval($value);
  }

  public function queryBoolValue(QueryBuilder $queryBuilder): bool|null
  {
    $value = $this->querySingleValue($queryBuilder);

    return is_null($value) ? null : $queryBuilder->boolval($value);
  }

  public function querySingleValue(QueryBuilder $queryBuilder): mixed
  {
    $resultValue = $this->querySingleRow($queryBuilder);

    if ($resultValue->notFound()) {
      throw new DatabaseValueNotFoundException("Single value not found. Query comment [{$queryBuilder->getComment()}]");
    }

    $value = (array)$resultValue->getValue();

    return reset($value);
  }

  public function querySingleRow(QueryBuilder $queryBuilder): QueryResultValue
  {
    $rowGenerator = $this->query($queryBuilder);

    return $this->createQuerySingleResultValue($rowGenerator);
  }

  public function startTransaction(): void
  {
    ++$this->txCounter;

    if ($this->txCounter === self::TX_COUNTER_INITIAL_VALUE + 1) {
      $this->resetRollback();
      $this->startTransactionImplementation();
    }
  }

  public function commit(): void
  {
    --$this->txCounter;
    if ($this->txCounter < self::TX_COUNTER_INITIAL_VALUE) {
      throw new DatabaseTransactionException('Commit before transaction start.');
    }

    if ($this->txCounter === self::TX_COUNTER_INITIAL_VALUE && $this->hasRolledback === false) {
      $this->commitImplementation();
    }

    if ($this->txCounter === self::TX_COUNTER_INITIAL_VALUE && $this->hasRolledback === true) {
      $this->rollback();
      $this->resetRollback();
      $this->resetTxCounter();

      // phpcs:ignore Generic.Files.LineLength.TooLong
      throw new DatabaseTransactionException('One of previous nested transactions did rollback. Now commit is not allowed.');
    }
  }

  public function rollback(): void
  {
    --$this->txCounter;

    $this->markRollback();
    if ($this->txCounter === self::TX_COUNTER_INITIAL_VALUE) {
      $this->rollbackImplementation();
    }
  }

  public function isInsideTransaction(): bool
  {
    return $this->txCounter > self::TX_COUNTER_INITIAL_VALUE;
  }

  private function resetTxCounter(): void
  {
    $this->txCounter = self::TX_COUNTER_INITIAL_VALUE;
  }

  public function forceRollback(): void
  {
    $this->markRollback();
    $this->rollbackImplementation();
  }

  private function resetRollback(): void
  {
    $this->hasRolledback = false;
  }

  private function markRollback(): void
  {
    $this->hasRolledback = true;
  }

  abstract protected function startTransactionImplementation(): void;
  abstract protected function commitImplementation(): void;
  abstract protected function rollbackImplementation(): void;
}
