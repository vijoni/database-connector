<?php

declare(strict_types=1);

namespace Vijoni\Database\QueryBuilder;

class PgQueryBuilder extends QueryBuilder
{
  protected function escapeBoolean(mixed $value): string
  {
    if ($value === null) {
      return $this->nullValue();
    }

    return $this->boolval($value) ? 'TRUE' : 'FALSE';
  }

  protected function escapeLiteral(string|null $value): string
  {
    if ($value === null) {
      return $this->nullValue();
    }

    /** @phpstan-ignore-next-line */
    return pg_escape_literal($this->connection, $value);
  }

  protected function escapeIdentifier(string $identifier): string
  {
    /** @phpstan-ignore-next-line */
    return pg_escape_identifier($this->connection, $identifier);
  }

  protected function nullValue(): string
  {
    return 'NULL';
  }
}
