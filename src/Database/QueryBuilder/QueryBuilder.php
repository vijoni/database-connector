<?php

declare(strict_types=1);

namespace Vijoni\Database\QueryBuilder;

use Vijoni\Database\Exception\QueryBuilderException;

abstract class QueryBuilder
{
  private static int $maxParamLength = 0;

  public function __construct( // @phpstan-ignore-line
    protected $connection, // @phpstan-ignore-line
    protected string $query = '',
    protected array $replaceValues = [],
    protected array $aliases = [],
    protected string $comment = ''
  ) {
    self::$maxParamLength = strlen('%t?');
  }

  public function build(): string
  {
    $query = str_replace(array_keys($this->aliases), array_values($this->aliases), $this->query);
    $query = $this->replaceParams($query, $this->replaceValues);

    return $this->renderComment() . $query;
  }

  private function replaceParams(string $query, array $replaceValues): string
  {
    $firstKey = array_key_first($replaceValues);
    if (is_string($firstKey)) {
      return $this->replaceParamsByStringKey($query, $replaceValues);
    }

    return $this->replaceParamsByPosition($query, $replaceValues);
  }

  public function getComment(): string
  {
    return $this->comment;
  }

  private function renderComment(): string
  {
    if (empty($this->comment)) {
      return '';
    }

    return $this->buildComment($this->comment) . "\n";
  }

  protected function buildComment(string $comment): string
  {
    return "-- {$comment}";
  }

  private function replaceParamsByPosition(string $providedQuery, array $replaceValues): string
  {
    $offset = 0;
    $query = '';
    $replaceIndex = 0;

    while ($paramPosition = strpos($providedQuery, '%', $offset)) {
      $query .= substr($providedQuery, $offset, $paramPosition - $offset);
      $queryLeftover = substr($providedQuery, $paramPosition);

      [$replaceParam, $replaceValue] = $this->readEscapeParam($queryLeftover);
      if ($replaceParam === '') {
        [$replaceParam, $replaceValue, $replaceIndex] =
          $this->readSequenceParam($queryLeftover, $replaceValues, $replaceIndex);
      }

      $query .= $replaceValue;
      $offset = $paramPosition + strlen($replaceParam);
      if ($offset === $paramPosition) {
        ++$offset;
      }
    }

    $query .= substr($providedQuery, $offset);

    return $query;
  }

  private function replaceParamsByStringKey(string $providedQuery, array $replaceValues): string
  {
    $offset = 0;
    $query = '';

    uksort($replaceValues, static fn ($keyA, $keyB) => strlen($keyA) - strlen($keyB));

    while ($paramPosition = strpos($providedQuery, '%', $offset)) {
      $query .= substr($providedQuery, $offset, $paramPosition - $offset);
      $queryLeftover = substr($providedQuery, $paramPosition);

      [$replaceParam, $replaceValue] = $this->readEscapeParam($queryLeftover);
      if ($replaceParam === '') {
        [$replaceParam, $replaceValue] = $this->readNamedParam($queryLeftover, $replaceValues);
      }

      $query .= $replaceValue;
      $offset = $paramPosition + strlen($replaceParam);
      if ($offset === $paramPosition) {
        ++$offset;
      }
    }

    $query .= substr($providedQuery, $offset);

    return $query;
  }

  private function readEscapeParam(string $query): array
  {
    if (str_starts_with($query, '%%')) {
      return ['%%', '%'];
    }

    return ['', ''];
  }

  private function readSequenceParam(string $query, array $replaceValues, int $replaceIndex): array
  {
    for ($paramLength = self::$maxParamLength; $paramLength > 1; $paramLength--) {
      $replaceParam = substr($query, 0, $paramLength);
      $callback = $this->readReplaceParamCallback($replaceParam);

      if ($callback === null) {
        continue;
      }

      $replaceValue = $this->callbackOrNull($replaceValues[$replaceIndex], $callback);

      return [$replaceParam, $replaceValue, ++$replaceIndex];
    }

    return ['', '', $replaceIndex];
  }

  private function readNamedParam(string $query, array $replaceValues): array
  {
    $paramVariant = strtok($query, '_');
    if ($paramVariant === false) {
      throw new QueryBuilderException("Expected named param syntax [%s_name]. Error at [$query]");
    }

    $queryLeftover = substr($query, strlen($paramVariant) + 1);
    $replaceKey = $this->findReplaceNamedKey($queryLeftover, $replaceValues);

    $callback = $this->readReplaceParamCallback($paramVariant);
    if ($callback === null) {
      throw new QueryBuilderException("Replace parameter variant not found: [{$paramVariant}]");
    }

    $replaceParam = "{$paramVariant}_{$replaceKey}";
    $replaceValue = $this->callbackOrNull($replaceValues[$replaceKey], $callback);

    return [$replaceParam, $replaceValue];
  }

  private function callbackOrNull(mixed $value, callable $callback): mixed
  {
    if ($value === null) {
      return $this->nullValue();
    }

    return $callback($value);
  }

  private function findReplaceNamedKey(string $query, array $replaceValues): string
  {
    foreach ($replaceValues as $replaceKey => $replaceValue) {
      if (str_starts_with($query, $replaceKey)) {
        return $replaceKey;
      }
    }

    throw new QueryBuilderException('Did not find a matching replace key');
  }

  private function readReplaceParamCallback(string $param): callable|null
  {
    $map = $this->paramsMap();
    if (key_exists($param, $map)) {
      return $map[$param];
    }

    return null;
  }

  private function paramsMap(): array
  {
    return [
      '%i' => fn ($value) => $this->escapeInteger($value),
      '%s' => fn ($value) => $this->escapeLiteral($value),
      '%d' => fn ($value) => $this->escapeFloat($value),
      '%b' => fn ($value) => $this->escapeBoolean($value),
      '%c' => fn ($value) => $this->escapeIdentifier($value),
      '%t?' => fn ($value) => $this->escapeByType($value),
      '%li' => fn (array $list) => $this->buildIntegerList($list),
      '%ls' => fn (array $list) => $this->buildStringList($list),
      '%ld' => fn (array $list) => $this->buildFloatList($list),
      '%lb' => fn (array $list) => $this->buildBooleanList($list),
      '%l?' => fn (array $list) => $this->buildListByType($list),
      '%lc' => fn (array $list) => $this->buildIdentifierList($list),
      '%lu' => fn (array $list) => $this->buildColumnValueList($list),
    ];
  }

  public function boolval(mixed $value): bool
  {
    $returnMap = [
      't' => true,
      'f' => false,
    ];

    if (isset($returnMap[$value])) {
      return $returnMap[$value];
    }

    return filter_var($value, FILTER_VALIDATE_BOOLEAN);
  }

  protected function escapeInteger(mixed $value): int|string
  {
    if ($value === null) {
      return $this->nullValue();
    }

    return intval($value);
  }

  protected function escapeFloat(mixed $value): float|string
  {
    if ($value === null) {
      return $this->nullValue();
    }

    return floatval($value);
  }

  protected function escapeByType(mixed $value): mixed
  {
    if (is_string($value)) {
      return $this->escapeLiteral($value);
    }

    if (is_bool($value)) {
      return $this->escapeBoolean($value);
    }

    if (is_float($value)) {
      return floatval($value);
    }

    if (is_int($value)) {
      return intval($value);
    }

    if ($value === null) {
      return $this->nullValue();
    }

    $valueType = gettype($value);
    throw new QueryBuilderException("Unhandled type: [{$valueType}]");
  }

  private function buildQueryList(array $list, callable $escapeFunction): string
  {
    $listRepresentation = '';
    $comma = '';
    foreach ($list as $value) {
      $listRepresentation .= $comma . $escapeFunction($value);
      $comma = ', ';
    }

    return $listRepresentation;
  }

  protected function buildIntegerList(array $list): string
  {
    return $this->buildQueryList($list, [$this, 'escapeInteger']);
  }

  protected function buildFloatList(array $list): string
  {
    return $this->buildQueryList($list, [$this, 'escapeFloat']);
  }

  protected function buildBooleanList(array $list): string
  {
    return $this->buildQueryList($list, [$this, 'escapeBoolean']);
  }

  protected function buildStringList(array $list): string
  {
    return $this->buildQueryList($list, [$this, 'escapeLiteral']);
  }

  protected function buildIdentifierList(array $list): string
  {
    return $this->buildQueryList($list, [$this, 'escapeIdentifier']);
  }

  protected function buildListByType(array $list): string
  {
    return $this->buildQueryList($list, [$this, 'escapeByType']);
  }

  private function buildColumnValueList(array $list): string
  {
    $listRepresentation = '';
    $comma = '';
    foreach ($list as $column => $value) {
      $listRepresentation .= $comma . $this->escapeIdentifier($column) . ' = ' . $this->escapeByType($value);
      $comma = ', ';
    }

    return $listRepresentation;
  }

  abstract protected function escapeBoolean(mixed $value): string;

  abstract protected function escapeLiteral(string $value): string;

  abstract protected function escapeIdentifier(string $identifier): string;

  abstract protected function nullValue(): string;
}
