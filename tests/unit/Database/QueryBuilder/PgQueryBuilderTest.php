<?php

declare(strict_types=1);

namespace VijoniTest\Unit\Database\Query;

use Vijoni\Database\QueryBuilder\PgQueryBuilder;

class PgQueryBuilderTest extends \Codeception\Test\Unit
{
  public function testBuildQuery(): void
  {
    $queryBuilder = new PgQueryBuilder(null, 'SELECT * FROM product;');
    $queryResult = $queryBuilder->build();

    $this->assertEquals('SELECT * FROM product;', $queryResult);
  }

  public function testBuildQueryGivenComment(): void
  {
    $queryBuilder = new PgQueryBuilder(null, 'SELECT * FROM product;', [], [], 'get all products');
    $queryResult = $queryBuilder->build();

    $this->assertEquals("-- get all products\nSELECT * FROM product;", $queryResult);
  }

  /**
   * @dataProvider buildQueryGivenSingleParamCases
   */
  public function testBuildQueryGivenSingleParam(string $param, mixed $value, string $expected): void
  {
    $queryBuilder = new PgQueryBuilder(null, "SELECT * FROM product WHERE some_column = {$param};", [$value]);
    $queryResult = $queryBuilder->build();

    $this->assertEquals("SELECT * FROM product WHERE some_column = {$expected};", $queryResult);
  }

  public function buildQueryGivenSingleParamCases(): array
  {
    return [
      'escape' => ['%%', '%', '%'],
      'integer' => ['%i', 21, '21'],
      'integer null' => ['%i', null, 'NULL'],
      'float' => ['%d', 699.2354, '699.2354'],
      'float null' => ['%d', null, 'NULL'],
      'bool true' => ['%b', true, 'TRUE'],
      'bool false' => ['%b', false, 'FALSE'],
      'bool yes' => ['%b', 'yes', 'TRUE'],
      'bool no' => ['%b', 'no', 'FALSE'],
      'bool 1' => ['%b', 1, 'TRUE'],
      'bool 0' => ['%b', 0, 'FALSE'],
      'bool null' => ['%b', null, 'NULL'],
      'by value type - int' => ['%t?', 21, '21'],
      'by value type - float' => ['%t?', 699.2354, '699.2354'],
      'by value type - bool' => ['%t?', true, 'TRUE'],
      'by value type - null' => ['%t?', null, 'NULL'],
    ];
  }

  /**
   * @dataProvider buildQueryGivenListParamCases
   */
  public function testBuildQueryGivenListParam(string $param, mixed $value, string $expected): void
  {
    $queryBuilder = new PgQueryBuilder(null, "SELECT * FROM product WHERE some_column IN ({$param});", [$value]);
    $queryResult = $queryBuilder->build();

    $this->assertEquals("SELECT * FROM product WHERE some_column IN ({$expected});", $queryResult);
  }

  public function buildQueryGivenListParamCases(): array
  {
    return [
      'integer list' => ['%li', [12, 21, 54, null], '12, 21, 54, NULL'],
      'float list' => ['%ld', [699.2354, 234.12, null], '699.2354, 234.12, NULL'],
      'bool list' => ['%lb', [true, false, 1, 0, 'yes', 'no', null], 'TRUE, FALSE, TRUE, FALSE, TRUE, FALSE, NULL'],
      'type list' => ['%l?', [true, false, 12, 234.12, null], 'TRUE, FALSE, 12, 234.12, NULL'],
    ];
  }

  public function testBuildQueryGivenManyParams(): void
  {
    $query = "SELECT * FROM product WHERE rank = %i AND price > %d AND is_active = %b AND name LIKE('%%bob%%');";

    $queryBuilder = new PgQueryBuilder(null, $query, [21, 60.253, true]);
    $queryResult = $queryBuilder->build();

    $expected = "SELECT * FROM product WHERE rank = 21 AND price > 60.253 AND is_active = TRUE AND name LIKE('%bob%');";
    $this->assertEquals($expected, $queryResult);
  }

  public function testBuildQueryGivenSingleNamedParam(): void
  {
    $query = "SELECT * FROM product WHERE age = %d_age;";
    $queryBuilder = new PgQueryBuilder(null, $query, ['age' => 21]);
    $queryResult = $queryBuilder->build();

    $this->assertEquals("SELECT * FROM product WHERE age = 21;", $queryResult);
  }

  public function testBuildQueryGivenManyNamedParams(): void
  {
    $query = <<<SQL
SELECT * FROM product
    WHERE rank = %i_rank AND price > %d_price AND is_active = %b_is_active AND name LIKE('%%bob%%');
SQL;

    $queryBuilder = new PgQueryBuilder(null, $query, ['rank' => 21, 'price' => 60.253, 'is_active' => true]);
    $queryResult = $queryBuilder->build();

    $expectedQuery = <<<SQL
SELECT * FROM product
    WHERE rank = 21 AND price > 60.253 AND is_active = TRUE AND name LIKE('%bob%');
SQL;

    $this->assertEquals($expectedQuery, $queryResult);
  }

  public function testBuildQueryGivenOneAlias(): void
  {
    $query = "SELECT 'constant' as __alias;";
    $queryBuilder = new PgQueryBuilder(null, $query, [], ['__alias' => 'myalias']);
    $queryResult = $queryBuilder->build();

    $expectedQuery = "SELECT 'constant' as myalias;";
    $this->assertEquals($expectedQuery, $queryResult);
  }

  public function testBuildQueryGivenManyAlias(): void
  {
    $query = "SELECT 'constant' as __alias_one, 'constant' as __alias_two;";
    $queryBuilder = new PgQueryBuilder(
      null,
      $query,
      [],
      [
        '__alias_one' => 'myalias_one',
        '__alias_two' => 'myalias_two',
      ]
    );
    $queryResult = $queryBuilder->build();

    $expectedQuery = "SELECT 'constant' as myalias_one, 'constant' as myalias_two;";
    $this->assertEquals($expectedQuery, $queryResult);
  }

  public function testBuildQueryGivenRepeatingAlias(): void
  {
    $query = "SELECT 'constant' as __product_id, 'constant' as __alias_two FROM product __product_id;";
    $queryBuilder = new PgQueryBuilder(
      null,
      $query,
      [],
      [
        '__product_id' => 'product_id',
        '__alias_two' => 'myalias_two',
      ]
    );
    $queryResult = $queryBuilder->build();

    $expectedQuery = "SELECT 'constant' as product_id, 'constant' as myalias_two FROM product product_id;";
    $this->assertEquals($expectedQuery, $queryResult);
  }
}
