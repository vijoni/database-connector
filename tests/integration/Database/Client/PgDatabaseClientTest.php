<?php

declare(strict_types=1);

namespace VijoniTest\Integration\Database\Client;

use Vijoni\Database\Exception\DatabaseException;
use Vijoni\Database\Exception\DatabaseTransactionException;
use Vijoni\Database\Exception\DatabaseValueNotFoundException;
use VijoniTest\IntegrationTester;

class PgDatabaseClientTest extends \Codeception\Test\Unit
{
  protected IntegrationTester $tester;

  private bool $existsTemporaryTable = false;

  protected function _before(): void
  {
    if (!$this->existsTemporaryTable) {
      $this->createTemporaryTable();
    }
  }

  public function testQueryNative(): void
  {
    $dbClient = $this->tester->dbClient();

    $rowGenerator = $dbClient->queryNative("SELECT 'some random string';", 'query native single row test');
    $row = $rowGenerator->current();

    $this->assertEquals(['?column?' => 'some random string'], $row);
  }

  public function testQueryNativeManyRows(): void
  {
    $dbClient = $this->tester->dbClient();
    $faker = $this->tester->faker();
    $fakeNameA = $faker->name();
    $fakeNameB = $faker->name();

    $this->insertTemporaryTableValueAt(1000, 'name', $fakeNameA);
    $this->insertTemporaryTableValueAt(1001, 'name', $fakeNameB);

    $query = 'SELECT name FROM tmp_table_name WHERE id IN (1000, 1001)';
    $rowGenerator = $dbClient->queryNative($query, 'query native many rows test');
    $readRows = iterator_to_array($rowGenerator, true);

    $this->assertEquals(
      [
        ['name' => $fakeNameA],
        ['name' => $fakeNameB]
      ],
      $readRows
    );
  }

  public function testQueryNativeAll(): void
  {
    $dbClient = $this->tester->dbClient();
    $faker = $this->tester->faker();
    $fakeNameA = $faker->name();
    $fakeNameB = $faker->name();

    $this->insertTemporaryTableValueAt(1002, 'name', $fakeNameA);
    $this->insertTemporaryTableValueAt(1003, 'name', $fakeNameB);

    $query = 'SELECT name FROM tmp_table_name WHERE id IN (1002, 1003)';
    $allRows = $dbClient->queryNativeAll($query, 'query native all rows test');

    $this->assertEquals(
      [
        ['name' => $fakeNameA],
        ['name' => $fakeNameB]
      ],
      $allRows
    );
  }

  public function testQuery(): void
  {
    $dbClient = $this->tester->dbClient();

    $queryBuilder = $dbClient->newQueryBuilder("SELECT 'some random string';", [], [], 'query native single row test');
    $rowGenerator = $dbClient->query($queryBuilder);
    $row = $rowGenerator->current();

    $this->assertEquals(['?column?' => 'some random string'], $row);
  }

  public function testQueryManyRows(): void
  {
    $dbClient = $this->tester->dbClient();
    $faker = $this->tester->faker();
    $fakeNameA = $faker->name();
    $fakeNameB = $faker->name();

    $this->insertTemporaryTableValueAt(1004, 'name', $fakeNameA);
    $this->insertTemporaryTableValueAt(1005, 'name', $fakeNameB);

    $query = 'SELECT name FROM tmp_table_name WHERE id IN (1004, 1005)';
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query many rows test');
    $rowGenerator = $dbClient->query($queryBuilder);
    $readRows = iterator_to_array($rowGenerator, true);

    $this->assertEquals(
      [
        ['name' => $fakeNameA],
        ['name' => $fakeNameB]
      ],
      $readRows
    );
  }

  public function testQueryAll(): void
  {
    $dbClient = $this->tester->dbClient();
    $faker = $this->tester->faker();
    $fakeNameA = $faker->name();
    $fakeNameB = $faker->name();

    $this->insertTemporaryTableValueAt(1006, 'name', $fakeNameA);
    $this->insertTemporaryTableValueAt(1007, 'name', $fakeNameB);

    $query = 'SELECT name FROM tmp_table_name WHERE id IN (1006, 1007)';
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query all rows test');
    $allRows = $dbClient->queryAll($queryBuilder);

    $this->assertEquals(
      [
        ['name' => $fakeNameA],
        ['name' => $fakeNameB]
      ],
      $allRows
    );
  }

  public function testQuerySingleValue(): void
  {
    $fakeName = $this->tester->faker()->name();
    $this->insertTemporaryTableValueAt(10, 'name', $fakeName);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT name FROM tmp_table_name WHERE id=10";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], "query single value test [{$fakeName}]");
    $readFakeName = $dbClient->querySingleValue($queryBuilder);

    $this->assertSame($fakeName, $readFakeName);
  }

  public function testQuerySingleNullValue(): void
  {
    $this->insertTemporaryTableValueAt(11, 'name', null);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT name FROM tmp_table_name WHERE id=11";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query single null value test');
    $readNullValue = $dbClient->querySingleValue($queryBuilder);

    $this->assertNull($readNullValue);
  }

  public function testQueryNonExistingSingleValue(): void
  {
    $this->insertTemporaryTableValueAt(12, 'name', null);
    $dbClient = $this->tester->dbClient();

    $this->expectException(DatabaseValueNotFoundException::class);
    $query = "SELECT name FROM tmp_table_name WHERE id=999";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query single value not found test');
    $dbClient->querySingleValue($queryBuilder);
  }

  public function testQueryString(): void
  {
    $fakeName = $this->tester->faker()->name();
    $this->insertTemporaryTableValueAt(21, 'name', $fakeName);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT name FROM tmp_table_name WHERE id=21";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], "query string value test [{$fakeName}]");
    $readFakeName = $dbClient->queryStringValue($queryBuilder);

    $this->assertSame($fakeName, $readFakeName);
  }

  public function testQueryStringNullValue(): void
  {
    $this->insertTemporaryTableValueAt(22, 'name', null);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT name FROM tmp_table_name WHERE id=22";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query string null value test');
    $readNullValue = $dbClient->queryStringValue($queryBuilder);

    $this->assertNull($readNullValue);
  }

  public function testQueryBooleanTrue(): void
  {
    $this->insertTemporaryTableValueAt(30, 'is_married', '1');
    $dbClient = $this->tester->dbClient();

    $query = "SELECT is_married FROM tmp_table_name WHERE id=30";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query true value test');

    $readTrueValue = $dbClient->queryBoolValue($queryBuilder);

    $this->assertTrue($readTrueValue);
  }

  public function testQueryBooleanFalse(): void
  {
    $this->insertTemporaryTableValueAt(31, 'is_married', '0');
    $dbClient = $this->tester->dbClient();

    $query = "SELECT is_married FROM tmp_table_name WHERE id=31";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query false value test');

    $readFalseValue = $dbClient->queryBoolValue($queryBuilder);

    $this->assertFalse($readFalseValue);
  }

  public function testQueryBooleanNullValue(): void
  {
    $this->insertTemporaryTableValueAt(32, 'is_married', null);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT is_married FROM tmp_table_name WHERE id=32";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query boolean null value test');
    $readNullValue = $dbClient->queryBoolValue($queryBuilder);

    $this->assertNull($readNullValue);
  }

  public function testQueryInt(): void
  {
    $fakeAge = $this->tester->faker()->numberBetween(1, 99);
    $this->insertTemporaryTableValueAt(40, 'age', $fakeAge);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT age FROM tmp_table_name WHERE id=40";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], "query int value test [{$fakeAge}]");
    $readFakeAge = $dbClient->queryIntValue($queryBuilder);

    $this->assertSame($fakeAge, $readFakeAge);
  }

  public function testQueryIntNullValue(): void
  {
    $this->insertTemporaryTableValueAt(41, 'age', null);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT age FROM tmp_table_name WHERE id=41";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query int null value test');
    $readNullValue = $dbClient->queryIntValue($queryBuilder);

    $this->assertNull($readNullValue);
  }

  public function testQueryFloat(): void
  {
    $fakePrice = $this->tester->faker()->randomFloat(4);
    $this->insertTemporaryTableValueAt(50, 'price', $fakePrice);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT price FROM tmp_table_name WHERE id=50";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], "query float value test [{$fakePrice}]");
    $readFakePrice = $dbClient->queryFloatValue($queryBuilder);

    $this->assertSame($fakePrice, $readFakePrice);
  }

  public function testQueryFloatNullValue(): void
  {
    $this->insertTemporaryTableValueAt(51, 'price', null);
    $dbClient = $this->tester->dbClient();

    $query = "SELECT price FROM tmp_table_name WHERE id=51";
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query float null value test');
    $readNullValue = $dbClient->queryFloatValue($queryBuilder);

    $this->assertNull($readNullValue);
  }

  public function boolvalCases(): array
  {
    return [
      "'t' -> true" => ['t', true],
      "'f' -> false" => ['l', false],
      "'yes' -> true" => ['yes', true],
      "'no' -> false" => ['no', false],
      "'1' -> true" => ['1', true],
      "'0' -> false" => ['0', false],
      "'true' -> true" => ['true', true],
      "'false' -> false" => ['false', false],
      "true -> true" => [true, true],
      "false -> false" => [false, false],
    ];
  }

  public function testQuerySingleRow(): void
  {
    $dbClient = $this->tester->dbClient();

    $faker = $this->tester->faker();
    $fakeName = $faker->name();
    $fakeAge = $faker->numberBetween(0, 99);
    $fakePrice = $faker->randomFloat(4);
    $fakeIsMarried = 'yes';

    $query = 'INSERT INTO tmp_table_name VALUES (200, %s, %i, %d, %b);';
    $qb = $dbClient->newQueryBuilder(
      $query,
      [$fakeName, $fakeAge, $fakePrice, $fakeIsMarried],
      [],
      'query first row test'
    );
    $dbClient->query($qb);

    $query = 'SELECT * FROM tmp_table_name WHERE id=200';
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query first row test');
    $queryResultValue = $dbClient->querySingleRow($queryBuilder);
    $row = $queryResultValue->getValue();

    $this->assertEquals([
      'id' => 200,
      'name' => $fakeName,
      'age' => strval($fakeAge),
      'price' => strval($fakePrice),
      'is_married' => 't',
    ], $row);
  }

  public function testQuerySingleRowNotFound(): void
  {
    $dbClient = $this->tester->dbClient();

    $query = 'SELECT * FROM tmp_table_name WHERE id=999';
    $queryBuilder = $dbClient->newQueryBuilder($query, [], [], 'query first row test');
    $queryResultValue = $dbClient->querySingleRow($queryBuilder);

    $this->assertTrue($queryResultValue->notFound());
  }

  public function testCommitTransaction(): void
  {
    $dbClient = $this->tester->dbClient();

    $fakeName = $this->tester->faker()->name();

    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(100, 'name', $fakeName);
    $dbClient->commit();

    $rows = $this->fetchNamesBy('id=100');
    $this->assertEquals($fakeName, $rows[0]['name']);
  }

  public function testRollbackTransaction(): void
  {
    $dbClient = $this->tester->dbClient();

    $fakeName = $this->tester->faker()->name();

    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(101, 'name', $fakeName);
    $dbClient->rollback();

    $rows = $this->fetchNamesBy('id=101');
    $this->assertEmpty($rows);
  }

  public function testCommitNestedTransaction(): void
  {
    $dbClient = $this->tester->dbClient();

    $fakeNameA = $this->tester->faker()->name();
    $fakeNameB = $this->tester->faker()->name();

    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(102, 'name', $fakeNameA);
    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(103, 'name', $fakeNameB);
    $dbClient->commit();
    $dbClient->commit();

    $rows = $this->fetchNamesBy('id IN (102, 103)');

    $readFakeNameA = $rows[0]['name'];
    $readFakeNameB = $rows[1]['name'];
    $this->assertEquals($fakeNameA, $readFakeNameA);
    $this->assertEquals($fakeNameB, $readFakeNameB);
  }

  public function testRollbackNestedTransaction(): void
  {
    $dbClient = $this->tester->dbClient();

    $fakeNameA = $this->tester->faker()->name();
    $fakeNameB = $this->tester->faker()->name();

    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(104, 'name', $fakeNameA);
    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(105, 'name', $fakeNameB);
    $dbClient->rollback();
    $dbClient->rollback();

    $rows = $this->fetchNamesBy('id IN (104, 105)');
    $this->assertEmpty($rows);
  }

  public function testCommitRollbackedTransaction(): void
  {
    $dbClient = $this->tester->dbClient();
    $this->expectException(DatabaseTransactionException::class);
    $this->expectExceptionMessage('One of previous nested transactions did rollback. Now commit is not allowed.');

    $fakeNameA = $this->tester->faker()->name();
    $fakeNameB = $this->tester->faker()->name();

    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(106, 'name', $fakeNameA);
    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(107, 'name', $fakeNameB);
    $dbClient->rollback();
    $dbClient->commit();
  }

  public function testRollbackCommittedTransaction(): void
  {
    $dbClient = $this->tester->dbClient();

    $fakeNameA = $this->tester->faker()->name();
    $fakeNameB = $this->tester->faker()->name();

    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(108, 'name', $fakeNameA);
    $dbClient->startTransaction();
    $this->insertTemporaryTableValueAt(109, 'name', $fakeNameB);
    $dbClient->commit();
    $dbClient->rollback();

    $rows = $this->fetchNamesBy('id IN (108, 109)');
    $this->assertEmpty($rows);
  }

  public function testSimultaneousQueryResults(): void
  {
    $dbClient = $this->tester->dbClient();

    $faker = $this->tester->faker();
    $fakeNameAA = $faker->name();
    $fakeNameAB = $faker->name();
    $fakeNameC = $faker->name();

    $this->insertTemporaryTableValueAt(2000, 'name', $fakeNameAA);
    $this->insertTemporaryTableValueAt(2001, 'name', $fakeNameAB);
    $this->insertTemporaryTableValueAt(2002, 'name', $fakeNameC);

    $rowGeneratorA =  $dbClient->queryNative('SELECT name FROM tmp_table_name WHERE id IN(2000, 2001)');
    $rowGeneratorC =  $dbClient->queryNative('SELECT name FROM tmp_table_name WHERE id IN(2002)');

    $this->assertSame($fakeNameAA, $rowGeneratorA->current()['name']);
    $this->assertSame($fakeNameC, $rowGeneratorC->current()['name']);

    $rowGeneratorA->next();
    $this->assertSame($fakeNameAB, $rowGeneratorA->current()['name']);
  }

  public function testQueryErrorReporting(): void
  {
    $dbClient = $this->tester->dbClient();
    $this->expectException(DatabaseException::class);
    $this->expectExceptionMessageRegExp('/column "unknown" does not exist/');
    $dbClient->queryNative('SELECT unknown FROM tmp_table_name;');
  }

  private function createTemporaryTable(): void
  {
    $dbClient = $this->tester->dbClient();
    $query = <<<SQL
CREATE TEMPORARY TABLE IF NOT EXISTS tmp_table_name(
  id int primary key,
  name varchar,
  age int,
  price float,
  is_married bool
);
SQL;
    $dbClient->queryNative($query, 'create temporary test table');
  }

  private function insertTemporaryTableValueAt(int $index, string $column, mixed $value): void
  {
    $dbClient = $this->tester->dbClient();
    $query = "INSERT INTO tmp_table_name (id, {$column}) VALUES ({$index}, %t?)";

    $queryBuilder = $dbClient->newQueryBuilder($query, [$value], [], 'set provided test temporary table value');
    $dbClient->query($queryBuilder);
  }

  private function fetchNamesBy(string $where): array
  {
    $dbClient = $this->tester->dbClient();
    $query = "SELECT name FROM tmp_table_name WHERE {$where}";

    return $dbClient->queryNativeAll($query, 'fetch test temporary table value');
  }
}
