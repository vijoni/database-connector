<?php

declare(strict_types=1);

namespace VijoniTest\Integration\Database\Client;

use Vijoni\Database\Client\PgDatabaseClient;
use VijoniTest\IntegrationTester;

class PgDatabaseClientPoolTest extends \Codeception\Test\Unit
{
  protected IntegrationTester $tester;

  public function testMasterOnlyConnection(): void
  {
    $dbUrl = 'pgsql://postgres:postgres@localhost:5432/postgres';
    $dbClient = new PgDatabaseClient($dbUrl);
    $dbClient->init();

    $fakeName = $this->tester->faker()->name();
    $qb = $dbClient->newQueryBuilder('SET SESSION my.name TO %s;', [$fakeName], [], 'test master only connection');
    $dbClient->query($qb);
    $queryBuilder = $dbClient->newQueryBuilder("SELECT current_setting('my.name');");
    $readName = $dbClient->queryStringValue($queryBuilder);
    $this->assertSame($fakeName, $readName);

    $dbClient->useWriteConnection();
    $readName = $dbClient->queryStringValue($queryBuilder);
    $this->assertSame($fakeName, $readName);
  }

  public function testMasterSlaveSeparatedConnection(): void
  {
    $dbUrl = 'pgsql://postgres:postgres@localhost:5432/postgres';
    $dbClient = new PgDatabaseClient($dbUrl, $dbUrl);
    $dbClient->init();

    $fakeName = $this->tester->faker()->name();
    $query = 'SET SESSION my.name TO %s;';
    $qb = $dbClient->newQueryBuilder($query, [$fakeName], [], 'test master/slave connection isolation');
    $dbClient->query($qb);
    $queryBuilder = $dbClient->newQueryBuilder("SELECT current_setting('my.name');");

    $readName = $dbClient->queryStringValue($queryBuilder);
    $this->assertSame($fakeName, $readName);

    $dbClient->useWriteConnection();
    $this->expectExceptionMessageRegExp('/unrecognized configuration parameter "my.name"/');
    $dbClient->queryStringValue($queryBuilder);
  }
}
