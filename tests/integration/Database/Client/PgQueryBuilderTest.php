<?php

declare(strict_types=1);

namespace VijoniTest\Integration\Database\Client;

use Vijoni\Database\QueryBuilder\PgQueryBuilder;
use VijoniTest\IntegrationTester;

class PgQueryBuilderTest extends \Codeception\Test\Unit
{
  protected IntegrationTester $tester;

  public function testBuildQueryGivenSingleStringParam(): void
  {
    $dbClient = $this->tester->dbClient();
    $fakeName = $this->tester->faker()->name();

    $queryBuilder = new PgQueryBuilder($dbClient->con(), "SELECT * FROM product WHERE name = %s;", [$fakeName]);
    $queryResult = $queryBuilder->build();

    $e = static fn (string $input) => str_replace("'", "''", $input);
    $this->assertEquals("SELECT * FROM product WHERE name = '{$e($fakeName)}';", $queryResult);
  }

  public function testBuildQueryGivenSingleIdentifierParam(): void
  {
    $dbClient = $this->tester->dbClient();

    $queryBuilder = new PgQueryBuilder($dbClient->con(), "SELECT %c FROM product WHERE name = 'toy_car';", ['price']);
    $queryResult = $queryBuilder->build();

    $this->assertEquals('SELECT "price" FROM product WHERE name = \'toy_car\';', $queryResult);
  }

  public function testBuildQueryGivenManyIdentifierParams(): void
  {
    $dbClient = $this->tester->dbClient();

    $query = "SELECT %c, %c FROM product WHERE name = 'toy_car';";
    $queryBuilder = new PgQueryBuilder($dbClient->con(), $query, ['price', 'name']);
    $queryResult = $queryBuilder->build();

    $this->assertEquals('SELECT "price", "name" FROM product WHERE name = \'toy_car\';', $queryResult);
  }

  public function testBuildQueryGivenManyStringParams(): void
  {
    $dbClient = $this->tester->dbClient();
    $faker = $this->tester->faker();
    $fakeFirstName = $faker->firstName();
    $fakeLastName = $faker->lastName();

    $query = "SELECT * FROM user WHERE firstname = %s AND lastname = %s;";
    $queryBuilder = new PgQueryBuilder($dbClient->con(), $query, [$fakeFirstName, $fakeLastName]);
    $queryResult = $queryBuilder->build();

    $e = static fn (string $input) => str_replace(";", "''", $input);
    $expectedQuery = <<<SQL
SELECT * FROM user WHERE firstname = '{$e($fakeFirstName)}' AND lastname = '{$e($fakeLastName)}';
SQL;
    $this->assertEquals($expectedQuery, $queryResult);
  }

  public function testBuildQueryGivenStringListParam(): void
  {
    $dbClient = $this->tester->dbClient();
    $faker = $this->tester->faker();
    $fakeNameA = $faker->name();
    $fakeNameB = $faker->name();

    $query = "SELECT * FROM user WHERE name IN (%ls);";
    $queryBuilder = new PgQueryBuilder($dbClient->con(), $query, [[$fakeNameA, $fakeNameB]]);
    $queryResult = $queryBuilder->build();

    $e = static fn (string $input) => str_replace(";", "''", $input);
    $expectedQuery = "SELECT * FROM user WHERE name IN ('{$e($fakeNameA)}', '{$e($fakeNameB)}');";
    $this->assertEquals($expectedQuery, $queryResult);
  }

  public function testBuildQueryGivenColumnListParam(): void
  {
    $dbClient = $this->tester->dbClient();

    $query = "SELECT %lc FROM user WHERE name IN ('Konrad', 'Thomas');";
    $queryBuilder = new PgQueryBuilder($dbClient->con(), $query, [['name', 'age']]);
    $queryResult = $queryBuilder->build();

    $expectedQuery = 'SELECT "name", "age" FROM user WHERE name IN (\'Konrad\', \'Thomas\');';
    $this->assertEquals($expectedQuery, $queryResult);
  }

  public function testBuildQueryGivenColumnValueListParam(): void
  {
    $dbClient = $this->tester->dbClient();

    $query = "UPDATE user SET %lu WHERE name = 'Konrad';";
    $queryBuilder = new PgQueryBuilder($dbClient->con(), $query, [['title' => 'Mr.', 'age' => 21]]);
    $queryResult = $queryBuilder->build();

    $expectedQuery = 'UPDATE user SET "title" = \'Mr.\', "age" = 21 WHERE name = \'Konrad\';';
    $this->assertEquals($expectedQuery, $queryResult);
  }
}
