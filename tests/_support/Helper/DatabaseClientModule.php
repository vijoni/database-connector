<?php

declare(strict_types=1);

namespace VijoniTest\Helper;

use Codeception\Module;
use Vijoni\Database\Client\DatabaseClient;
use Vijoni\Database\Client\PgDatabaseClient;
use VijoniTest\Database\DbInit;

class DatabaseClientModule extends Module
{
  /**
   * @var string[]
   */
  protected $requiredFields = ['url'];

  /**
   * @var string[]
   */
  protected $config = ['url'];

  private DatabaseClient $dbClient;

  public function _initialize(): void
  {
    $dbInit = new DbInit();
    $testDbName = $dbInit->createTestDatabase();
    $dbUrl = rtrim($this->config['url'], '/') . "/{$testDbName}";

    $dbClient = new PgDatabaseClient($dbUrl, $dbUrl);
    $dbClient->init();

    $dbInit->init($dbClient);

    $this->dbClient = $dbClient;
  }

  public function dbClient(): DatabaseClient
  {
    return $this->dbClient;
  }
}
