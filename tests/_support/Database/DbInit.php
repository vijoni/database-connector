<?php

declare(strict_types=1);

namespace VijoniTest\Database;

use DateTime;
use Vijoni\Database\Client\DatabaseClient;
use Vijoni\Database\Client\PgDatabaseClient;

class DbInit
{
  public function init(DatabaseClient $dbClient): void
  {
    $this->createProductTable($dbClient);
  }

  public function createTestDatabase(): string
  {
    $dbUrl = 'pgsql://postgres:postgres@localhost:5432/postgres';
    $dbClient = new PgDatabaseClient($dbUrl, $dbUrl);
    $dbClient->init();

    $dateSuffix = (new DateTime())->format('YmdHis_u');
    $testDatabaseName = 'database_connector_test_' . $dateSuffix . '_' . rand();

    $query = <<<SQL
CREATE DATABASE {$testDatabaseName}
ENCODING=UTF8
LC_COLLATE='en_US.utf8'
LC_CTYPE='en_US.utf8'
OWNER=postgres
TEMPLATE=template0;
SQL;

    $dbClient->queryNative($query, "create test database [{$testDatabaseName}]");

    return $testDatabaseName;
  }

  private function createProductTable(DatabaseClient $dbClient): void
  {
    $query = <<<SQL
CREATE UNLOGGED TABLE IF NOT EXISTS product (
    dbid bigserial PRIMARY KEY,
    id varchar NOT NULL UNIQUE,
    name varchar NOT NULL,
    price int NOT NULL
);
SQL;

    $dbClient->queryNative($query, 'create test product table');
  }
}
